package com.dd.trieu.bai11;

import java.util.PriorityQueue;
import java.util.Queue;

public class VD1 implements Comparable<VD1> {
    private int id;
    private String name, author, publisher;
    private int quantity;

    public VD1(int id, String name, String author, String publisher, int quantity) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.quantity = quantity;
    }

    @Override
    public int compareTo(VD1 b) {
        if (this.id > b.id) {
            return 1;
        } else if (this.id < b.id) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Book [id=" + id + ", name=" + name + ", author=" + author
                + ", publisher=" + publisher + ", quantity=" + quantity + "]";
    }

}

class PriorityQueueDemo2 {
    public static void main(String[] args) {
        // Init PriorityQueue
        Queue<VD1> queue = new PriorityQueue<>();
        // Creating Books
        VD1 b1 = new VD1(121, "Let us C", "Yashwant Kanetkar", "BPB", 8);
        VD1 b2 = new VD1(233, "Operating System", "Galvin", "Wiley", 6);
        VD1 b3 = new VD1(101, "Data Communications & Networking", "Forouzan", "Mc Graw Hill", 4);
        // Adding Books to the queue
        queue.add(b1);
        queue.add(b2);
        queue.add(b3);
        System.out.println("Traversing the queue elements:");
        // Traversing queue elements
        for (VD1 b : queue) {
            System.out.println(b);
        }
        System.out.println("After removing one book record:");
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
    }
}