package com.dd.trieu.bai3;

public class Linearsearch {

    // doi cho truc tiep
    private static void interchangeSort(int [] array ){
        for( int i = 0; i< array.length-1; i++){
            for( int j = 1+i; j< array.length; j++ )
                if( array[i] > array[j] ){
                    int temp  = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
        }
    }
    // sap xep noi bot
    static void bubbleSort( int [] array ){
        for( int i = 0; i< array.length-1; i++){
            for( int j = array.length-1; j> i; j-- )
                if( array[j-1] > array[j] ){
                    int temp  = array[j];
                    array[j] = array[j-1];
                    array[j-1] = temp;
                }
        }
    }
    //tim kiem tuyen tinh
    private static boolean sequentialSearch(int [] array, int value ){ // linearSearch
        for( int i = 0; i< array.length; i++ )
            if( array[i] == value ) return true;
        return false;
    }

    private static void display(int [] array ){
        for( int element:array){
            System.out.print(" " + element);
        }
        System.out.println("");
    }

    private  static int [] getArray( int numOfElement ){
        int [] result = new int[numOfElement];
        for( int i = 0; i<numOfElement; i++ ){
            result[i] = numOfElement-i;
        }
        return result;
    }

    public static void main(String[] args) {
        int [] array;
        array = getArray(20);
        System.out.println("mang truoc khi sap xep: ");
        display(array);
        bubbleSort(array);
        System.out.println("gia tri 30 co trong mang array hay khong ? " + sequentialSearch(array, 30));
        System.out.println("gia tri 10 co trong mang array hay khong ? " + sequentialSearch(array, 10));
    }
}

