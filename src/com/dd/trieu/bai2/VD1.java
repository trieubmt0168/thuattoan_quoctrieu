package com.dd.trieu.bai2;

import java.util.Scanner;

public class VD1 {
    private static int[] a;
    private static int n;
    private static Scanner scan = new Scanner(System.in);


    public static void main(String[] args) {
        nhapMang();
        chenPT();
        printindexArray();
        deleteArray();
        xoaArr();
        updateIndexArray();
    }

    private static void nhapMang() {
        System.out.print("nhap so phan tu cua mang a[]: ");
        n = scan.nextInt();
        a = new int[n + 1];
        for (int i = 0; i < a.length - 1; i++) {
            System.out.print("nhap phan tu a[" + i + "]= ");
            a[i] = scan.nextInt();
        }

    }

    private static void chenPT() {
        int x;
        System.out.print("nhap phan tu can chen, x = ");
        x = scan.nextInt();
        chen(a, x);
    }

    private static void printindexArray() {
        System.out.println("tìm phần tử trong mảng: ");
        int k = scan.nextInt();
        for (int i = 0; i < a.length; i++) {
            if (k == a[i]) {
                System.out.printf("Mảng đc tìm thấy là = " + i, a[i] + 1);
            }
        }
    }

    private static void deleteArray() {
        int x;
        System.out.printf("nhap phan tu can xoa, x = ");
        x = scan.nextInt();
        chenXoa(a, x);
    }

    private static void xoaArr() {
        int x;
        System.out.printf("Nhaapj mangr can xoa, x = ");
        x = scan.nextInt();
        deleteArray(a, x);

    }

    private static void updateIndexArray() {
        int x;
        System.out.printf("Cap nhat lai phan tu, x = ");
        x = scan.nextInt();
        updateIndexArray(a, x, 4);
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////
    private static void chen(int[] a, int x) {
        if (a[0] > x) {
            for (int i = a.length - 1; i > 0; i--)
                a[i] = a[i - 1];
            a[0] = x;
        } else if (a[a.length - 2] < x) a[a.length - 1] = x;
        else {
            int index = 0;
            for (int i = 0; i < a.length - 1; i++)
                if (a[i] > x) {
                    index = i;
                    break;
                }
            for (int i = a.length - 1; i > index; i--)
                a[i] = a[i - 1];
            a[index] = x;
        }
        display(a);
    }

    private static void chenXoa(int[] arr, int index) {

        int i, j;
        System.out.println("Xóa vị trí của mảng : ");
        int newArr[] = new int[arr.length - 1];
        int deleteArr[] = new int[1];
        for (j = i = 0; i < arr.length; i++) {
            if (i != index) {
                newArr[j] = arr[i];
                j++;
            } else {
                deleteArr[0] = arr[i];
            }
        }
        for (int k = 0; k < newArr.length; k++) {
            System.out.println(" " + newArr[k]);
        }


    }

    private static void deleteArray(int[] arr, int element) {
        int i, j;
        System.out.println("Delete Array : ");
        int newArr[] = new int[arr.length - 1];
        int deleteArr[] = new int[1];
        for (j = i = 0; i < arr.length; i++) {
            if (arr[i] != element) {
                newArr[j] = arr[i];
                j++;
            } else {
                deleteArr[0] = arr[i];
            }
        }
        for (int c = 0; c < newArr.length; c++) {
            System.out.println(" " + newArr[c]);
        }
        System.out.println("Delete : " + deleteArr[0]);
    }

    private static void updateIndexArray(int[] arr, int index, int element) {
        int i;
        System.out.println("Update Index of Array : ");
        int newArr[] = new int[arr.length];
        for (i = 0; i < arr.length; i++) {
            if (i == index) {
                newArr[i] = element;
            } else {
                newArr[i] = arr[i];
            }
        }
        for (int c = 0; c < newArr.length; c++) {
            System.out.println(" " + newArr[c]);
        }
    }

    private static void display(int[] a) {
        System.out.print("mang a[]: ");
        for (int x : a)
            System.out.print(x + " ");
        System.out.println("");
    }
}
