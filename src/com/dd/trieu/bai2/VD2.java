package com.dd.trieu.bai2;

import java.util.Arrays;
import java.util.Scanner;

public class VD2 {
    public static Scanner scanner = new Scanner(System.in);
    static int arrayInt[] = { 9, 24, 1, 52, 18 };
    public static void main(String[] args) {
        printArray(arrayInt);
//		pushToArray(arrayInt, 8);
//		deleteByIndex(arrayInt, 1);
//		deleteArray(arrayInt, 18);
        findArray(arrayInt, 24);
        updateIndexArray(arrayInt, 0, 99);
        updateArray(arrayInt,24,99);
    }

    public static void printArray(int[] arr) {
        System.out.println("Array Length : " + arr.length);
        for (int i = 0; i < arr.length; i++) {

            System.out.println(" " + arrayInt[i]);
        }
    }

    public static void pushToArray(int[] arr, int element) {
        System.out.println("Push to Array : ");
        int newArr[] = new int[arr.length + 1];
        for (int i = 0; i < arr.length; i++) {
            newArr[i] = arr[i];
        }
        newArr[arr.length] = element;
        for (int i = 0; i < newArr.length; i++) {
            System.out.println(" " + newArr[i]);
        }
    }

    public static void deleteByIndex(int[] arr, int index) {
        int i, j;
        System.out.println("Delete Index of Array : ");
        int newArr[] = new int[arr.length - 1];
        int deleteArr[] = new int[1];
        for (j = i = 0; i < arr.length; i++) {
            if (i != index) {
                newArr[j] = arr[i];
                j++;
            } else {
                deleteArr[0] = arr[i];
            }
        }
        for (int c = 0; c < newArr.length; c++) {
            System.out.println(" " + newArr[c]);
        }
        System.out.println("Delete : " + deleteArr[0]);
    }

    public static void deleteArray(int[] arr, int element) {
        int i, j;
        System.out.println("Delete Array : ");
        int newArr[] = new int[arr.length - 1];
        int deleteArr[] = new int[1];
        for (j = i = 0; i < arr.length; i++) {
            if (arr[i] != element) {
                newArr[j] = arr[i];
                j++;
            } else {
                deleteArr[0] = arr[i];
            }
        }
        for (int c = 0; c < newArr.length; c++) {
            System.out.println(" " + newArr[c]);
        }
        System.out.println("Delete : " + deleteArr[0]);
    }

    public static void findArray(int[] arr, int element) {
        int i;
        int newArr[] = new int[1];
        for (i = 0; i < arr.length; i++) {
            if (arr[i] == element) {
                newArr[0] = arr[i];
            }
        }
        System.out.println("Find Index of Array : " + newArr[0]);
    }

    public static void updateIndexArray(int[] arr, int index, int element) {
        int i;
        System.out.println("Update Index of Array : ");
        int newArr[] = new int[arr.length];
        for (i = 0; i < arr.length; i++) {
            if (i == index) {
                newArr[i] = element;
            }else {
                newArr[i] = arr[i];
            }
        }
        for (int c = 0; c < newArr.length; c++) {
            System.out.println(" " + newArr[c]);
        }
    }

    public static void updateArray(int[] arr, int oldElement, int newElement) {
        int i;
        System.out.println("Update Index of Array : ");
        int newArr[] = new int[arr.length];
        for (i = 0; i < arr.length; i++) {
            if (arr[i] == oldElement) {
                newArr[i] = newElement;
            }else {
                newArr[i] = arr[i];
            }
        }
        for (int c = 0; c < newArr.length; c++) {
            System.out.println(" " + newArr[c]);
        }
    }
}
