package com.dd.trieu.bai4;

import java.util.Scanner;

public class VD1 { public static void main(String[] args) {
    printIndex();
}

    private static void printIndex() {

        int[] mang = new int[5];
        int n,m;

        Scanner scan = new Scanner(System.in);
        System.out.println("Nhap vao mang la: ");
        for (int i = 0; i < mang.length; i++) {
            mang[i] = scan.nextInt();
        }
        System.out.println("Mang nhap vao la :");
        for (int x : mang) {
            System.out.println(x);
        }

        System.out.println("");
        System.out.println("************************");

        for (int i = 0; i < mang.length - 1; i++) {
            int so = mang[i];
            for (int j = i + 1; j < mang.length; j++) {
                if (so > mang[j]) {
                    mang[i] = mang[j];
                    mang[j] = so;
                    so = mang[i];
                }
            }

        }
        System.out.println("************************");
        System.out.println("Sáp xếp tang dần");
        for (int x : mang) {
            System.out.println(x);
        }
        for (int i = 0; i < mang.length - 1; i++) {
            int so = mang[i];
            for (int j = i + 1; j < mang.length; j++) {
                if (so < mang[j]) {
                    mang[i] = mang[j];
                    mang[j] = so;
                    so = mang[i];
                }
            }

        }
        System.out.println("Mảng giảm dần ");
        for (int s : mang) {
            System.out.println(s);
        }

    }


}

