//package com.dd.trieu.bai6;
//
//import com.dd.trieu.bai12entity.Product;
//
//import java.util.ArrayList;
//
//public class VD3 {
//    public static void getSort(ArrayList<Product> array) {
//        int n = array.length;
//        for (int j = 1; j < n; j++) {
//            int key = array[j];
//            int i = j - 1;
//            while ((i > -1) && (array[i] > key)) {
//                array[i + 1] = array[i];
//                i--;
//            }
//            array[i + 1] = key;
//        }
//
//    }
//
//    private static void insertionSort() {
//        int[] arr1 = {9, 14, 3, 2, 43, 11, 58, 22};
//        System.out.println("Trước khi dùng ");
//        for (int i : arr1) {
//            System.out.print(i + " ");
//        }
//        System.out.println();
//
//        getSort(arr1);
//
//        System.out.println("Sau khi dùng");
//        for (int i : arr1) {
//            System.out.print(i + " ");
//        }
//    }
//
//    public static void main(String a[]) {
//        insertionSort();
//    }
//}
