package com.dd.trieu.bai13;

import com.dd.trieu.bai12.Database;
import com.dd.trieu.bai12entity.Category;
import com.dd.trieu.bai12entity.Product;

import java.util.ArrayList;
import java.util.List;

public class Bai13 {
    static Database database = new Database();
    private static ArrayList<Product> productList = new ArrayList<>();
    private static ArrayList<Category> categoryList = new ArrayList<>();

    public static void main(String[] args) {
        createProductTest();
        printProduct("product");
        printProductbyCategory(productList, 1);

    }

    public static void createProductTest() {

        productList.add(new Product("CPU", 10, 750, 1));
        System.out.println("");
        productList.add(new Product("Main", 2, 200, 2));
        productList.add(new Product("RAM", 2, 50, 2));
        productList.add(new Product("HHD", 8, 28, 1));
        productList.add(new Product("SSD", 50, 500, 4));
        productList.add(new Product("KeyBoard", 60, 100, 4));
        productList.add(new Product("Mouse", 40, 50, 4));
        productList.add(new Product("VGA", 3, 700, 3));
        productList.add(new Product("Monitor", 1, 322, 2));
        database.insertTable("productList", productList);
        System.out.printf(" " + productList);

        System.out.println(" ");
        System.out.printf("*******Cateogry********");
        categoryList.add(new Category(1, "Computer"));
        categoryList.add(new Category(2, "Memory"));
        categoryList.add(new Category(3, "Card"));
        categoryList.add(new Category(4, "Accessory"));
        database.insertTable("categoryList", categoryList);
        System.out.println("\n" + categoryList);

    }

    private static void printProduct(String name) {
        System.out.println("----- Xuất  tên ------");
        for (Product product : productList) {
            if ("VGA".equals(product.getName())) {
                System.out.println(product);
            }
        }


    }

    private static List<Product> printProductbyCategory(List<Product> productList, int catgoryId) {
        for (Product product : productList) {
            if (product.getCategoryId() == catgoryId) {
                System.out.println(product);
            }
        }
        return productList;
    }

}
