package com.dd.trieu.bai12;

import com.dd.trieu.bai12entity.Category;
import com.dd.trieu.bai12entity.Product;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class VDDao {
    public static void main(String[] args) {
        createProductTest();
        printProduct("product");
        List<Product> list = mapProductByCategory(productList, categoryList);
        System.out.println(list);
        System.out.println("******sắp xếp theo giá******");
        sortByPrice(productList);
        for (Product list1 : productList) {
            System.out.println(list1);

        }
        System.out.println("sắp xếp theo tên");
        sortByName(productList);
        for (Product list2 : productList) {
            System.out.println(list2);
        }
        System.out.println("Sắp xếp theo ");
        sortByCategoryName(productList);
        for (Product liProduct: productList){
            System.out.println(liProduct);
        }

    }

    static Database database = new Database();
    private static ArrayList<Product> productList = new ArrayList<>();
    private static ArrayList<Category> categoryList = new ArrayList<>();


    public static void createProductTest() {

        productList.add(new Product("CPU", 10, 750, 1));
        System.out.println("");
        productList.add(new Product("Main", 2, 200, 2));
        productList.add(new Product("RAM", 2, 50, 2));
        productList.add(new Product("HHD", 8, 28, 1));
        productList.add(new Product("SSD", 50, 500, 4));
        productList.add(new Product("KeyBoard", 60, 100, 4));
        productList.add(new Product("Mouse", 40, 50, 4));
        productList.add(new Product("VGA", 3, 700, 3));
        productList.add(new Product("Monitor", 1, 322, 2));
        database.insertTable("productList", productList);
        System.out.printf(" " + productList);

        System.out.println(" ");
        System.out.printf("*******Cateogry********");
        categoryList.add(new Category(1, "Computer"));
        categoryList.add(new Category(2, "Memory"));
        categoryList.add(new Category(3, "Card"));
        categoryList.add(new Category(4, "Accessory"));
        database.insertTable("categoryList", categoryList);
        System.out.println("\n" + categoryList);

    }

    private static void printProduct(String name) {
        System.out.println("----- Xuất  tên ------");
        List<Product> collect = new ArrayList<>();
        for (Product product : productList) {
            if ("VGA".equals(product.getName())) {
                System.out.println(product);
            }
        }


    }

    private static List<Product> mapProductByCategory(List<Product> listProduct, List<Category> listCategory) {
        System.out.println("Xuất category");
        List<Product> products = new ArrayList<Product>();
        for (Product product : listProduct) {
            for (Category category : listCategory) {
                if (product.getCategoryId() == category.getId()) {
                    product.setCategoryName(category.getName());
                }
            }
            products.add(product);
        }
        return products;
    }

    private static List<Product> sortByPrice(List<Product> listProduct) {
        List<Product> product = new LinkedList<Product>();
        for (int i = 0; i < listProduct.size() - 1; i++) {
            for (int j = i + 1; j < listProduct.size(); j++) {
                if (listProduct.get(i).getPrice() > listProduct.get(j).getPrice()) {
                    Product temp = listProduct.get(j);
                    listProduct.set(j, listProduct.get(i));
                    listProduct.set(i, temp);
                }

            }
        }
        return product;
    }

    private static List<Product> sortByName(List<Product> listProduct) {
        List<Product> product = new LinkedList<Product>();
        for (int i = 0; i < listProduct.size() - 1; i++) {
            for (int j = i + 1; j < listProduct.size(); j++) {
                if (listProduct.get(i).getName().length() > listProduct.get(j).getName().length()) {
                    Product temp = listProduct.get(j);
                    listProduct.set(j, listProduct.get(i));
                    listProduct.set(i, temp);
                }

            }
        }
        return product;
    }

    private static List<Product> sortByCategoryName(List<Product> productList1) {
        List<Product> product = new LinkedList<Product>();
        for (int i = 0; i < productList1.size() - 1; i++) {
            for (int j = i + 1; j < productList1.size(); j++) {
                if (productList1.get(i).getCategoryName().compareTo(productList1.get(j).getCategoryName()) > 0) {
                    Product temp = productList1.get(j);
                    productList1.set(j, productList1.get(i));
                    productList1.set(i, temp);
                }

            }
        }
        return product;
    }


}

