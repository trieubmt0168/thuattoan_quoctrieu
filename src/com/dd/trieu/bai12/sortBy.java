package com.dd.trieu.bai12;

import java.util.Arrays;
import java.util.Collections;

public class sortBy {
    public static void main(String[] args) {
        Integer [] arr = new Integer[]{1,3,2,4,5,7,10,15};
        String [] names= {"Nam","Dương","Trieu","Duy","An"};
        String res= Arrays.toString(names);
        System.out.println(res);
        Arrays.sort(arr);
        Arrays.sort(arr, Collections.reverseOrder());
        System.out.println("Sau khi sắp xếp : ");
        System.out.println(Arrays.toString(names));
        System.out.println(Arrays.toString(arr) );
    }
}
