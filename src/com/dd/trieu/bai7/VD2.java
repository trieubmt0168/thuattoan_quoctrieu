package com.dd.trieu.bai7;

public class VD2 {
    private static int arr[] = {64, 25, 12, 22, 11};

    public static void main(String[] args) {
        sort(arr);
        System.out.println("Sorted array");
        printArray(arr);
    }

    private static void sort(int arr[]) {
        int n = arr.length;

        for (int i = 0; i < n - 1; i++) {
            int min = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < arr[min]) {
                    min = j;
                }
                if (min != i) {
                    int t = arr[min];
                    arr[min] = arr[i];
                    arr[i] = t;
                }
            }
        }
    }
    private static void printArray(int arr[])
    {
        int n = arr.length;
        for (int i=0; i<n; ++i)
            System.out.print(arr[i]+" ");
        System.out.println();
    }

}
