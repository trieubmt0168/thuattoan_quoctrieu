package com.dd.trieu.bai10;

import java.util.Stack;

public class Vd1 {
    public static void main(String[] agrs) {
        getStack();
    }

    private static void getStack() {
        Stack<Integer> s = new Stack<Integer>();
        for (int i = 0; i < 16; i++)
            s.push(i);
        System.out.println("Index of number 6 in Stack : " + s.search(6));
        System.out.println("Index of number 15 in Stack : " + s.search(16));
        while (!s.isEmpty()) {
            System.out.print(s.peek() + "   ");
            s.pop();
        }
    }
}
